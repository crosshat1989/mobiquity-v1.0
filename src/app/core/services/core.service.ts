import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams, HttpErrorResponse} from '@angular/common/http';
import { RequestOptions } from '@angular/http';
import {Observable} from 'rxjs';
import { map, catchError } from 'rxjs/operators';
// import 'rxjs/add/operator/mergeMap';
// import 'rxjs/add/operator/switchMap';
// import 'rxjs/add/operator/catch';
@Injectable()

export class F1Service {
// For real project urls will be entered in the environment variables... 
  private url1 = "http://ergast.com/api/f1/"
  private url2 = "http://ergast.com/api/f1/driverStandings/1.json?limit=11&offset=55"

  public newData: any;
  public filters = [];
  public processedFilters = [];
  masterParam: string;
  inexFilter = [];
  constructor(private http: HttpClient) {}

  getSeason(year): Observable<any> {
  const result = this.http.get(this.url1 + year + '/results/1.json');
  return result.pipe(map((data: any) => {
    if (data === null) {
      alert('No Data!');
    }
      this.newData = data['MRData'];
      return this.newData;
  }))
  .pipe(catchError((err: HttpErrorResponse) => {
    alert('Internal Server Error or No data available!!');
    location.reload();
   return Observable.throw(err.statusText);
 }));
}

getWinner() {
  const result = this.http.get(this.url2);
  return result.pipe(map((data: any) => {
    if (data === null) {
      alert('No Data!');
    }
      this.newData = data['MRData'];
      return this.newData;
  }))
  .pipe(catchError((err: HttpErrorResponse) => {
    alert('Internal Server Error or No data available!!');
    location.reload();
   return Observable.throw(err.statusText);
 }));
}

}