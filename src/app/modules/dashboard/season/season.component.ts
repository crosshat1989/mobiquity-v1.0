import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { F1Service } from '../../../core/services/core.service';

@Component({
  selector: 'app-season',
  templateUrl: './season.component.html',
  styleUrls: ['./season.component.css'],
  providers: [F1Service]
})
export class SeasonComponent implements OnInit {
  loaded = false;
  data: any;
  winner: string;
  constructor(private f1Service: F1Service, public dialog: MatDialog, 
  public dialogRef: MatDialogRef<SeasonComponent>,
    @Inject(MAT_DIALOG_DATA) public data1: any) { }

  ngOnInit() {
    this.f1Service.getSeason(this.data1['season']).subscribe((data: any) => {
      this.data = data;
      this.winner = this.data1['winner'];
      this.loaded = true;
    })
  }

  closeDialog() {
    this.dialog.closeAll();
  }

}
