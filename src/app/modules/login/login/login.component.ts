import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

signinForm: FormGroup;

  constructor(private router: Router) { }

  login() {
  if (this.signinForm.value['username'] === "mobiquity" && this.signinForm.value['password'] === "f1") {
    localStorage.setItem("login", "true");
    this.router.navigate(['dashboard']);
  }
}

  ngOnInit() {
    this.signinForm = new FormGroup({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
      rememberMe: new FormControl(false)
    });
  }

}
